import { Component } from '@angular/core';
import { MatDialog } from '@angular/material'
import { DialogingresarComponent } from './dialogingresar/dialogingresar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test2angular';

  constructor(public dialog: MatDialog) {}

  openDialog(){
    this.dialog.open(DialogingresarComponent); 
  }
  
}
