import { Component, OnInit, ViewChild } from '@angular/core';
import { DialogingresarComponent } from 'src/app/dialogingresar/dialogingresar.component';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import {MatSnackBar} from '@angular/material'
import { Paciente } from 'src/app/model/paciente';
import { PacienteService } from 'src/app/services/paciente.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  actions: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0077, symbol: 'H', actions: 'd'},
  {position: 2, name: 'pichula', weight: 1.0467, symbol: 'g', actions: 'd'},
  {position: 3, name: 'wgwrgwrg', weight: 1.2459, symbol: 'jH', actions: 'd'},
  {position: 4, name: 'wrgwrgwn', weight: 1.0079, symbol: 'k', actions: 'd'}
];

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {
  edicion:boolean=false;
  paciente:Paciente;
  displayedColumns=['Enfermedad','id','nombre','rut','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource:MatTableDataSource<Paciente>;
  constructor(private pacienteService:PacienteService, public dialog: MatDialog, private snackBar: MatSnackBar) {}

  openDialog(){
    this.dialog.open(DialogingresarComponent); 
  }
  

  ngOnInit() {
    this.pacienteService.pacienteCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.pacienteService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    this.pacienteService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  eliminar(id:number){
    this.pacienteService.eliminar(id).subscribe(data => {
      this.pacienteService.listar().subscribe(data =>{
        this.pacienteService.pacienteCambio.next(data);
        this.pacienteService.mensajeCambio.next('Se elimino');
      });
    });
  }
}
