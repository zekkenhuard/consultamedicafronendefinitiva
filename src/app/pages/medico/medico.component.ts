import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { DialogmedicoComponent } from 'src/app/dialogs/dialogmedico/dialogmedico.component';
import {MatSnackBar} from '@angular/material'
import { Medico } from 'src/app/model/medico';
import { MedicoService } from 'src/app/services/medico.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  actions: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', actions: 'd'}
];

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  displayedColumns=['especialidad','id','nombre','rut','acciones'];
  dataSource:MatTableDataSource<Medico>;
  constructor(private medicoService:MedicoService, public dialog: MatDialog, private snackBar: MatSnackBar) {}

  openDialog(){
    this.dialog.open(DialogmedicoComponent); 
  }
  

  ngOnInit() {
    this.medicoService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
    })
  }
  delete(elm: PeriodicElement) {
   /* this.dataSource.data = this.dataSource.data
      .filter(i => i !== elm)
      .map((i, idx) => (i.position = (idx + 1), i));*/
  }
  
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
       duration: 2000,
    });
 } 
}
