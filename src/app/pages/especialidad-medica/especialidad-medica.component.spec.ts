import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecialidadMedicaComponent } from './especialidad-medica.component';

describe('EspecialidadMedicaComponent', () => {
  let component: EspecialidadMedicaComponent;
  let fixture: ComponentFixture<EspecialidadMedicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspecialidadMedicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecialidadMedicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
