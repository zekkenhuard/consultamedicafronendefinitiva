import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DialogespecialidadmedicaComponent } from 'src/app/dialogs/dialogespecialidadmedica/dialogespecialidadmedica.component';
import {MatSnackBar} from '@angular/material'
import { EspecialidadService } from 'src/app/services/especialidad.service';
import { Especialidad } from 'src/app/model/especialidad';

@Component({
  selector: 'app-especialidad-medica',
  templateUrl: './especialidad-medica.component.html',
  styleUrls: ['./especialidad-medica.component.css']
})

export class EspecialidadMedicaComponent implements OnInit {
  especialidad:Especialidad;
  edicion:boolean=false;
  dataSource:MatTableDataSource<Especialidad>;
  displayedColumns=['id','descripcion','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private especialidadService:EspecialidadService,public dialog: MatDialog, private snackBar: MatSnackBar) {}

  openDialog(){
    this.dialog.open(DialogespecialidadmedicaComponent); 
  }

  ngOnInit() {
    this.especialidadService.especialidadCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.especialidadService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    
    this.especialidadService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
        
    }
    eliminar(id:number){
      this.especialidadService.eliminar(id).subscribe(data => {
        this.especialidadService.listar().subscribe(data =>{
          this.especialidadService.especialidadCambio.next(data);
          this.especialidadService.mensajeCambio.next('Se elimino');
        });
      });
    }
    applyFilter(filterValue:string){
      filterValue=filterValue.trim();
      filterValue=filterValue.toLowerCase();
      this.dataSource.filter=filterValue;
    }
    
}
  
  


