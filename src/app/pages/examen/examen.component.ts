import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { DialogexamenComponent } from 'src/app/dialogs/dialogexamen/dialogexamen.component';
import {MatSnackBar} from '@angular/material'
import { ExamenService } from 'src/app/services/examen.service';
import { Examen } from 'src/app/model/examen';


@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.css']
})
export class ExamenComponent implements OnInit {
  examen:Examen;
  edicion:boolean=false;
  displayedColumns=['id', 'descripcion', 'acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource:MatTableDataSource<Examen>;
  constructor(private examenService: ExamenService, public dialog: MatDialog, private snackBar: MatSnackBar) {}

  openDialog(){
    this.dialog.open(DialogexamenComponent); 
  }

  ngOnInit() {
    this.examenService.examenCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.examenService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    
    this.examenService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }
  eliminar(id:number){
    this.examenService.eliminar(id).subscribe(data => {
      this.examenService.listar().subscribe(data =>{
        this.examenService.examenCambio.next(data);
        this.examenService.mensajeCambio.next('Se elimino');
      });
    });
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
}
