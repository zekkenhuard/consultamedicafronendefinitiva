import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { MatRadioModule } from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import { ExamenComponent } from './pages/examen/examen.component';
import { EspecialidadMedicaComponent } from './pages/especialidad-medica/especialidad-medica.component';
import { MedicoComponent } from './pages/medico/medico.component';
import {MatIconModule} from '@angular/material/icon';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RouterModule,Routes} from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import { LayoutModule } from '@angular/cdk/layout';
import { DialogingresarComponent } from './dialogingresar/dialogingresar.component';
import { DialogmedicoComponent } from './dialogs/dialogmedico/dialogmedico.component';
import { DialogespecialidadmedicaComponent } from './dialogs/dialogespecialidadmedica/dialogespecialidadmedica.component';
import { DialogexamenComponent } from './dialogs/dialogexamen/dialogexamen.component';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import { HttpClientModule } from '@angular/common/http';
import {MatPaginatorModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';


const routes: Routes = [

{
  path: 'paciente',
  component:PacienteComponent
},  
{
  path:'examnen',
  component:ExamenComponent
},
{
  path:'medico',
  component:MedicoComponent
},
{
  path:'especialidadmedica',
  component:EspecialidadMedicaComponent
},
{
  path:'inicio',
  component:InicioComponent
},

{ path:"especialidadmedica",component:EspecialidadMedicaComponent,children:[
  {
    path:'edicion/:id',component:DialogespecialidadmedicaComponent
  },
  {
    path:'nuevo',component:DialogespecialidadmedicaComponent
  }
]
},
{ path:"examnen",component:ExamenComponent,children:[
  {
    path:'edicion/:id',component:DialogexamenComponent
  },
  {
    path:'nuevo',component:DialogexamenComponent
  }
]
},
{ path:"paciente",component:DialogingresarComponent,children:[
  {
    path:'edicion/:id',component:DialogingresarComponent
  },
  {
    path:'nuevo',component:DialogingresarComponent
  }
]
},
{path:'login',
component:LoginComponent}, 
]

@NgModule({
  declarations: [
    AppComponent,
    PacienteComponent,
    ExamenComponent,
    EspecialidadMedicaComponent,
    MedicoComponent,
    InicioComponent,
    DialogingresarComponent,
    DialogmedicoComponent,
    DialogespecialidadmedicaComponent,
    DialogexamenComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatRadioModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatInputModule,
    MatSnackBarModule,
    RouterModule.forRoot(routes),
    MatSidenavModule,
    MatListModule,
    LayoutModule,
    MatTableModule,
    MatTooltipModule,
    HttpClientModule,
    MatPaginatorModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialogingresarComponent, DialogespecialidadmedicaComponent, DialogexamenComponent, DialogmedicoComponent],
  
  exports: [
    MatTableModule
    

  ]

})
export class AppModule { }
