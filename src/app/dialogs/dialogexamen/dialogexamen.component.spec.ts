import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogexamenComponent } from './dialogexamen.component';

describe('DialogexamenComponent', () => {
  let component: DialogexamenComponent;
  let fixture: ComponentFixture<DialogexamenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogexamenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogexamenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
