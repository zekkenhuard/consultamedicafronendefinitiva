import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Examen } from 'src/app/model/examen';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ExamenService } from 'src/app/services/examen.service';

@Component({
  selector: 'app-dialogexamen',
  templateUrl: './dialogexamen.component.html',
  styleUrls: ['./dialogexamen.component.css']
})
export class DialogexamenComponent implements OnInit {

  form:FormGroup;
  id:number;
  examnen:Examen;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private examenService:ExamenService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'descripcion':new FormControl(''),            
    }); 
  }
  initForm(){
    if(this.edicion){
      this.examenService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'descripcion':new FormControl(data.descripcion),
        });
      })
        
    }
  }
  ngOnInit() {
    this.examnen=new Examen();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.examnen.id=this.form.value['id'];
    this.examnen.descripcion=this.form.value['descripcion'];        
    if(this.edicion){

      this.examenService.modificar(this.examnen).subscribe(data=>
      {
        this.examenService.listar().subscribe(examenes => {this.examenService.examenCambio.next(examenes);       
        this.examenService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.examenService.registar(this.examnen).subscribe(data=>{
        this.examenService.listar().subscribe(examenes => {this.examenService.examenCambio.next(examenes);       
        this.examenService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['examnen']);
  }

}
