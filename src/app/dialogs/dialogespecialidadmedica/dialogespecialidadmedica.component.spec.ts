import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogespecialidadmedicaComponent } from './dialogespecialidadmedica.component';

describe('DialogespecialidadmedicaComponent', () => {
  let component: DialogespecialidadmedicaComponent;
  let fixture: ComponentFixture<DialogespecialidadmedicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogespecialidadmedicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogespecialidadmedicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
