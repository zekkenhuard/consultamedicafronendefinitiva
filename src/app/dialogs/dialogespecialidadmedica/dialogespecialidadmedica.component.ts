import { Component, OnInit } from '@angular/core';
import { Especialidad } from 'src/app/model/especialidad';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { EspecialidadService } from 'src/app/services/especialidad.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-dialogespecialidadmedica',
  templateUrl: './dialogespecialidadmedica.component.html',
  styleUrls: ['./dialogespecialidadmedica.component.css']
})
export class DialogespecialidadmedicaComponent implements OnInit {

  form:FormGroup;
  id:number;
  especialidad:Especialidad;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private especialidadService:EspecialidadService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'descripcion':new FormControl(''),            
    }); 
  }
  initForm(){
    if(this.edicion){
      this.especialidadService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'descripcion':new FormControl(data.descripcion),
        });
      })
        
    }
  }
  ngOnInit() {
    this.especialidad=new Especialidad();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.especialidad.id=this.form.value['id'];
    this.especialidad.descripcion=this.form.value['descripcion'];        
    if(this.edicion){

      this.especialidadService.modificar(this.especialidad).subscribe(data=>
      {
        this.especialidadService.listar().subscribe(especialidades => {this.especialidadService.especialidadCambio.next(especialidades);       
        this.especialidadService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.especialidadService.registar(this.especialidad).subscribe(data=>{
        this.especialidadService.listar().subscribe(especialidades => {this.especialidadService.especialidadCambio.next(especialidades);       
        this.especialidadService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['especialidadmedica']);
  }


}
