import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Paciente } from '../model/paciente';
import { PacienteService } from '../services/paciente.service';

@Component({
  selector: 'app-dialogingresar',
  templateUrl: './dialogingresar.component.html',
  styleUrls: ['./dialogingresar.component.css']
})
export class DialogingresarComponent implements OnInit {

  form:FormGroup;
  id:number;
  paciente:Paciente;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private pacienteService:PacienteService) { 
    this.form=new FormGroup({
      'id': new FormControl(0),
      'Enfermedad':new FormControl(''),  
      'nombre':new FormControl(''),  
      'rut':new FormControl(''),  
    }); 
  }
  initForm(){
    if(this.edicion){
      this.pacienteService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id': new FormControl(data.id),
          'Enfermedad':new FormControl(data.Enfermedad),
          'nombre':new FormControl(data.nombre),
          'rut':new FormControl(data.rut),
        });
      })
        
    }
  }
  ngOnInit() {
    this.paciente=new Paciente();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.paciente.id=this.form.value['id'];
    this.paciente.Enfermedad=this.form.value['Enfermedad'];      
    this.paciente.nombre=this.form.value['nombre'];   
    this.paciente.rut=this.form.value['rut'];     
    if(this.edicion){

      this.pacienteService.modificar(this.paciente).subscribe(data=>
      {
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.pacienteService.registar(this.paciente).subscribe(data=>{
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['paciente']);
  }


}
