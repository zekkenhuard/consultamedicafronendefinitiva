import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogingresarComponent } from './dialogingresar.component';

describe('DialogingresarComponent', () => {
  let component: DialogingresarComponent;
  let fixture: ComponentFixture<DialogingresarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogingresarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogingresarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
