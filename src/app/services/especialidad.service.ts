import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Especialidad } from '../model/especialidad';
import { HOST, TOKEN_NAME } from '../shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {
  especialidadCambio=new Subject<Especialidad[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/especialidadesmedicas`;
  constructor(private http:HttpClient) { }
  listar(){  
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;  
    return this.http.get<Especialidad[]>(this.url,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
}
  modificar(especialidad:Especialidad){    
    console.log(this.url);
    console.log(especialidad);
    return this.http.put(this.url,especialidad);
  }
  registar(especialidad:Especialidad){
    
    return this.http.post(this.url,especialidad);
  }
  listarPorId(id:number){
    
    return this.http.get<Especialidad>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}
