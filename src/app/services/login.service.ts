import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, TOKEN_NAME, TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD } from './../shared/var.constant';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService  {
url:string=`${HOST}/oauth/token`;
constructor(private http: HttpClient, private router: Router) { }
user:string;
login2(usuario: string, contrasena: string){
  const body = `grant_type=password&username=${encodeURIComponent(usuario)}&password=${encodeURIComponent(contrasena)}`;
  //this.user=usuario;
  return this.http.post(this.url, body, {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8').set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
  })
  
}
login(usuario: string, contrasena: string){
  const body = `grant_type=password&username=${encodeURIComponent(usuario)}&password=${encodeURIComponent(contrasena)}`;
  return this.http.post(this.url,body,{
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8').set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
  })/*.pipe(
    catchError(error=>{
      console.log('Handling error locally and rethrowing it...', error);
      return throwError(error);
    })
).subscribe(res=>console.log('Http Response ',res),
err=>console.log('Http Response ',err),
()=>console.log('Http request Complete')
);    */ 

}


cerrarSesion(){
  sessionStorage.clear();
  this.router.navigate(['login']);
}
estadoLogeado(){
    let token =sessionStorage.getItem(TOKEN_NAME);
    return token!=null;
}
}