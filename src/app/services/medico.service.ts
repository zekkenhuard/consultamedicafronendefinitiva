import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HOST } from '../shared/var.constant';
import { HttpClient } from '@angular/common/http';
import { Medico } from '../model/medico';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {
  medicoCambio=new Subject<Medico[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/Medicos`;
  constructor(private http:HttpClient) { }
  listar(){
     return this.http.get<Medico[]>(this.url);
  }
}
